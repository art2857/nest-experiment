module.exports = {
  setupFiles: ['<rootDir>/apps/*/test/setEnvVars.js'],
  silent: false,
  moduleFileExtensions: ['js', 'ts'],
  rootDir: '.',
  testRegex: '[.](spec|test).ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  coverageDirectory: './coverage',
  testEnvironment: 'node',
  roots: ['<rootDir>/']
};

// module.exports = {
//   setupFiles: ['./test/setEnvVars.js'],
//   moduleFileExtensions: ['js', 'json', 'ts'],
//   rootDir: '.',
//   maxWorkers: 1,
//   testEnvironment: "node",
//   testRegex: '.e2e-spec.ts$',
//   transform: {
//     '^.+\\.(t|j)s$': 'ts-jest',
//   },
//   globals:{
//     "ts-jest": {
//       tsconfig:"tsconfig.e2e.json",
//     },
//   },
// };
  